import React, { useState } from 'react';
import './App.css';

function App() {
  const [chatInput, setChatInput] = useState('');
  
  function handleSend() {
    if (chatInput.trim() !== '') {
      let chats = JSON.parse(localStorage.getItem('chats')) || [];
      chats.push(chatInput);
      localStorage.setItem('chats', JSON.stringify(chats));
      setChatInput('');
    }
  }
  
  function handleShowMessages() {
    let chats = JSON.parse(localStorage.getItem('chats')) || [];
    console.log('Chats:', chats);
  }
  
  return (
    <div className="App">
      <div className="content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eget est sed risus pharetra malesuada at ac arcu.</p>
        <p>Maecenas vitae ipsum consequat, convallis justo vitae, scelerisque arcu.</p>
        <p>Suspendisse potenti. Ut vulputate lorem id tellus lacinia ultrices.</p>
        <p>Vestibulum at dui nec nisi tristique cursus id a nisi.</p>
        <p>Curabitur ultricies libero non nisi volutpat, a vestibulum metus sagittis.</p>
      </div>
      <div className="chat-box">
        <input type="text" id="chat" value={chatInput} onChange={(e) => setChatInput(e.target.value)} />
        <button onClick={handleSend}>Send</button>
        <button onClick={handleShowMessages}>Show messages</button>
      </div>
    </div>
  );
}

export default App;
